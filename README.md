# CoinJar

The coin jar will only accept the latest US coinage and has a volume of 42 fluid ounces. Additionally, the jar has a counter to keep track of the total amount of money collected and can reset the count back to $0.00.

# .net core 5 required.
please make sure you have .net core 5 sdk before you can do below steps.

# How to Run Application in Visual Studio
Open the solution in visual studio.
Make sure that CoinJar.WebApi is set as startup project.
Build and run the application.
The application will open the intenet browser, the default will be a swagger document where you can able to test 3 endpoints

# How to Run Application in Visual Studio Code
Open the solution in visual studio Code.
Make sure that CoinJar.WebApi is set as startup project.
In Termial window run dotnet restore.
When restore is complete run dotnet build.
When build is done run dotnet run.
Open your favarote internet browser and goto http://localhost:5001/swagger 
Swagger document will have 3 endpoints and able test.



