﻿using CoinJar.Core.Dtos;
using CoinJar.Core.Interfaces;
using CoinJar.Core.Interfaces.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Mime;

namespace CoinJar.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CoinJarsController : ControllerBase
    {
        private readonly ICoinJar _coinJar;
        private readonly ILogger<CoinJarsController> _logger;
        public CoinJarsController(ICoinJar coinJar, ILogger<CoinJarsController> logger)
        {
            _coinJar = coinJar;
            _logger = logger;
        }

        /// <summary>
        /// Add Coin to coin jar using latest US coinage and has a volume of 42 fluid ounces .
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/CoinJars/addCoin
        ///     {        
        ///       "Amount": "4000",
        ///       "Volume": "42",    
        ///     }
        /// </remarks>
        /// <param name="request"></param>   
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost("addCoin")]
        public ActionResult AddCoin([FromBody] CoinRequest request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                _coinJar.AddCoin(request);
                var response = new
                {
                    Message = "Coin added successfully",
                    StatusCode = 201,
                    Description = "Created"
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ArgumentException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Get total total amount of money collected.
        /// </summary>
        /// <remarks>
        /// GET api/CoinJars/getTotalAmount
        /// </remarks>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("getTotalAmount")]
        public ActionResult GetTotalAmount()
        {
            try
            {
                var total = _coinJar.GetTotalAmount();
                var response = new
                {
                    Message = "get total amount successfully",
                    StatusCode = 200,
                    Description = "Get",
                    Total = total
                };
                return Ok(response);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ArgumentException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Reset the count back to $0.00.
        /// </summary>
        /// <remarks>
        /// GET api/CoinJars/reset
        /// </remarks>
        [HttpPost("reset")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Reset()
        {
            try
            {
                _coinJar.Reset();
                var response = new
                {
                    Message = "reset successfully",
                    StatusCode = 200,
                    Description = "Delete",
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ArgumentException(ex.Message, ex);
            }
        }

    }
}
