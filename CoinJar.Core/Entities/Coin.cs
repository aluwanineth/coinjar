﻿using CoinJar.Core.Shared;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CoinJar.Core.Entities
{
    public class Coin : BaseEntity
    {
        public decimal Amount { get; set; }

        [Range(42, 42, ErrorMessage = "The field {0} must be equal {42}.")]
        [Column(TypeName = "decimal(18,2)")]
        public decimal? Volume { get; set; }
    }
}
