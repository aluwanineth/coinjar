﻿using CoinJar.Core.Dtos;

namespace CoinJar.Core.Interfaces.Services
{
    public interface ICoinJar
    {
        void AddCoin(ICoin coin);
        decimal GetTotalAmount();
        void Reset();
    }
}
