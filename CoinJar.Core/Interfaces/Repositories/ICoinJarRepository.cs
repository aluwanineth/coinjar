﻿using CoinJar.Core.Dtos;
using CoinJar.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoinJar.Core.Interfaces.Repositories
{
    public interface ICoinJarRepository
    {
        Task AddCoin(Coin entity);
        decimal GetTotalAmount();
        Task Reset();

    }
}
