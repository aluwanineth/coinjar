﻿using CoinJar.Core.Dtos;
using CoinJar.Core.Entities;
using CoinJar.Core.Interfaces;
using CoinJar.Core.Interfaces.Repositories;
using CoinJar.Core.Interfaces.Services;
using System;

namespace CoinJar.Core.Services
{
    public sealed class CoinJarService : ICoinJar
    {
        private readonly ICoinJarRepository _coinJarRepository;
        public CoinJarService(ICoinJarRepository coinJarRepositor)
        {
            _coinJarRepository = coinJarRepositor;
        }
        public void AddCoin(ICoin coin)
        {
            var entity = new Coin
            { 
                Amount = coin.Amount,
                Volume = coin.Volume
            };
            _coinJarRepository.AddCoin(entity).Wait();
        }

        public decimal GetTotalAmount()
        {
            return _coinJarRepository.GetTotalAmount();
        }

        public void Reset()
        {
            _coinJarRepository.Reset();
        }
    }
}
