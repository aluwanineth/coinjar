﻿using CoinJar.Core.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace CoinJar.Core.Dtos
{
    public class CoinRequest : ICoin
    {
        [Required]
        public decimal Amount { get ; set; }

        [Required]
        [Range(42, 42, ErrorMessage = "The field {0} must be equal {42}.")]
        public decimal Volume { get ; set ; }
    }
}
