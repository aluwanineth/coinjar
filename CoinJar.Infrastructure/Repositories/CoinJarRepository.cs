﻿using CoinJar.Core.Entities;
using CoinJar.Core.Interfaces.Repositories;
using CoinJar.Infrastructure.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CoinJar.Infrastructure.Repositories
{
    public class CoinJarRepository : EfRepository<Coin>, ICoinJarRepository
    {
        public CoinJarRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task AddCoin(Coin entity)
        { 
            await Add(entity);
        }
        public decimal GetTotalAmount()
        {
            var data = _appDbContext.Coins.ToList();
            return data.Sum(s => (decimal)s.Amount);
        }

        public async Task Reset()
        {
            var coins = await _appDbContext.Coins.ToListAsync();

            foreach(var coin in coins)
            {
                await Delete(coin);
            }
        }
    }
}
